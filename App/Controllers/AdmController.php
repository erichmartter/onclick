<?php

namespace App\Controllers;

//os recursos do miniframework
use MF\Controller\Action;
use MF\Model\Container;

class AdmController extends Action
{
	public function admin()
	{

		session_start();
		$vendas = Container::getModel('Venda');
		$vendas = $vendas->getAllVendas();
		$usuarios = Container::getModel('Usuario');
		$usuarios = $usuarios->getAllUsuarios();
		$this->view->vendas = $vendas;
		$this->view->usuarios = $usuarios;
		$this->render('admin','layout_adm');
	}
	public function admVendas()
	{
		session_start();
		$vendas = Container::getModel('Venda');
		$vendas = $vendas->getAllVendas();
		$this->view->vendas = $vendas;
		$this->render('adm_vendas','layout_adm');
	}

	public function oi()
	{
		//Cron Job que funciona
		//wget --no-check-certificate /dev/null https://onclickgames.com/oi?key=705b10540f48d522a485a3600a6937dc
		$usuario = Container::getModel('Usuario');
		if(isset($_GET['key']) && $_GET['key'] == '705b10540f48d522a485a3600a6937dc'){

			$usuario->__set('id', '100');
			$usuario->__set('pontos', 5000);
			$usuario->adicionaPontos();
			header('Location: /');
		}else{
			$usuario->__set('id', '100');
			$usuario->__set('pontos', 1);
			$usuario->adicionaPontos();
			echo 'Na';
		}

	}
	
	public function aprovarPagamantoBoleto(){
		$usuario = Container::getModel('Usuario');
		$venda = Container::getModel('Venda');
		$afiliado = Container::getModel('Usuario');
		if(isset($_GET['key']) && $_GET['key'] == '705b10540f48d522a485a3600a6937dc'){
			if(isset($_GET['id_venda']) && isset($_GET['id_usuario'])){
				$venda->__set('idUsuario', $_GET['id_usuario']);
				$venda->__set('idVenda', $_GET['id_venda']);
				$venda->ativaVenda();
				$venda = $venda->getUsuarioVenda();
				$usuario->__set('id', $_GET['id_usuario']);
				$usuario->__set('pontos', $venda['valor'] * 10);
				$usuario->adicionaPontos();
				$afiliado->__set('hash', $venda['afiliado']);
				$afiliado->__set('saldo', $venda['valor'] * 8);
				$afiliado->adicionaSaldoAfiliado();
				echo 'Venda ativada com sucesso!<br>
					Saldo de afiliado (se tiver) inserido com sucesso<br>
					  Pontos do Usuário inseridos com sucesso<br>
					  ';
			}else{
				echo 'Faltando algum dado';
			}
		}else{
			echo 'Erro na identificação';
		}
	}

	public function salvarJogo(){
		$this->validaAutenticacao();
		$jogo =	Container::getModel('Jogo');
		$jogo->__set('nomeJogo',$_POST['nome_jogo']);
		$jogo->__set('descJogo',$_POST['desc_jogo']);
		$jogo->salvar();
		header('Location: /adm_jogos');
	}

	public function validaAutenticacao()
	{
		session_start();

		if (!isset($_SESSION['id']) || $_SESSION['nivel'] != '2' || !isset($_SESSION['nome']) || $_SESSION['nome'] == '') {
			header('Location: /?login=erro');
		}
	}

}
