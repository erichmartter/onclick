<?php

namespace App\Controllers;

//os recursos do miniframework
use MF\Controller\Action;
use MF\Model\Container;
use MercadoPago;

class AppController extends Action
{

	public function meusJogos()
	{

		$this->validaAutenticacao();

		//recuperação dos jogos do Usuário
		$usuario = Container::getModel('Usuario');
		$vendaSteam = Container::getModel('Venda');
		$vendaEpic = Container::getModel('Venda');

		$vendaSteam->__set('idUsuario', $_SESSION['id']);
		$vendaSteam->__set('plataforma', 'Steam');
		$vendaEpic->__set('idUsuario', $_SESSION['id']);
		$vendaEpic->__set('plataforma', 'Epic Games');
		
		$this->view->caminho = 'img/bg-img/';
		$usuario->__set('id', $_SESSION['id']);

		$this->view->vendas = $vendaSteam->getAllUsuarioVenda();
		$this->view->vendasPendentes = $vendaSteam->getAllUsuarioVendaPendenteBoleto();
		$this->view->vendasEpic = $vendaEpic->getAllUsuarioVenda();
		$this->view->vendasPendentesEpic = $vendaEpic->getAllUsuarioVendaPendenteBoleto();
		$this->view->info_usuario = $usuario->getInfoUsuario();
		$this->view->totalJogos = count($vendaEpic->getAllUsuarioVenda()) + count($vendaSteam->getAllUsuarioVenda());

		$this->render('meus_jogos');
	}

	public function meuPerfil()
	{

		$this->validaAutenticacao();

		//recuperação dos jogos do Usuário
		$usuario = Container::getModel('Usuario');

		$this->view->caminho = 'img/bg-img/';
		$usuario->__set('id', $_SESSION['id']);

		$this->view->info_usuario = $usuario->getInfoUsuario();

		$this->render('meu_perfil');
	}

	public function verificaImap()
	{
		if (!function_exists('imap_open')) {
			echo "Não configurado.";
			exit();
		}
	}

	public function subtraiCodigo($idVenda, $limiteCod, $novoCodigo, $ultimoCodigo)
	{
		date_default_timezone_set('America/Sao_Paulo');
		$venda = Container::getModel('Venda');
		$venda->__set('idUsuario', $_SESSION['id']);
		$venda->__set('penultimoCod', $ultimoCodigo);
		$venda->__set('ultimoCod', $novoCodigo);
		$venda->__set('idVenda', $idVenda);
		$venda->__set('dataUltimoCod', date('Y-m-d H:i:s'));
		$venda->__set('limiteCod', $limiteCod - 1);
		$venda = $venda->subtraiCodigoSalvar();
	}



	public function verificaCupom()
	{
		$this->validaAutenticacao();
		if (isset($_GET['cupom'])) {
			$cupom = Container::getModel('Cupom');
			$game = Container::getModel('Jogo');
			$game->__set('nomeJogo', str_replace('-', ' ', $_SESSION['item']));
			$game = $game->getJogoByName();
			$_SESSION['gameId'] = $game['id_jogo'];
			$cupom->__set('conteudo', $_GET['cupom']);
			if ($cupom->getCupom()) {
				$cupom = $cupom->getCupom();
				foreach ($cupom as $key => $value) {
					if($value['fk_id_jogo'] == $game['id_jogo']){
						$_SESSION['desconto'] = $value['desconto'] / 100;
						echo '/pagamento?item=' . str_replace(' ', '-', $_SESSION['item']) . '&cupom=success';
						exit;
					}
				}
					echo '/pagamento?item=' . str_replace(' ', '-', $_SESSION['item']) . '&cupom=restrito';
					$_SESSION['desconto'] = 0;
			} else {
				echo '/pagamento?item=' . str_replace(' ', '-', $_SESSION['item']) . '&cupom=invalido';
			}
		}
	}

	public function pagamento()
	{
		$this->validaAutenticacao();
		
		if (!isset($_SESSION['id'])) {
			$this->view->login = isset($_GET['login']) ? $_GET['login'] : '';
			header('Location: /entrar?login=logar');
		} else {
			if (!isset($_GET['item']) || $_GET['item'] == '') {
				header('Location: /loja');
			} else {
				$produto = $_GET['item'];
			}
			$games = array();
			$game = Container::getModel('Jogo');
			$game->__set('nomeJogo', str_replace('-', ' ', $produto));
			$games = $game->getAllSearch();
			if (!isset($games[0])) {
				header('Location: /loja');
			}
			$this->view->games = $games;
			$this->view->key = $this->getPublicKey();
			$producao = $this->getAccessToken();
			$this->view->producao = $producao[2]; 
			unset($_SESSION['total']);
			unset($_SESSION['item']);
			if (isset($_SESSION['desconto'])) {
				$aux = $games[0]['valor'] * $_SESSION['desconto'];
				$_SESSION['total'] = $games[0]['valor'] - $aux;
			} else {
				$_SESSION['total'] = $games[0]['valor'];
			}
			if (isset($_COOKIE["affiliateOCG"])) {
				if ($_COOKIE["affiliateOCG"] == $_SESSION['hash']) {
					$this->view->afiliado = 'erro';
				} else {
					$this->view->afiliado = 'sucesso';
				}
			}
			$this->render('pagamento');
			$_SESSION['item'] = str_replace('-', ' ', $produto);
		}
	}

	public function adicionaSaldoAfiliado()
	{
		if (isset($_COOKIE["affiliateOCG"])) {
			$afiliado = Container::getModel('Usuario');
			$afiliado->__set('hash', $_COOKIE["affiliateOCG"]);
			if ($afiliado->getUsuarioPorHash()) {
				if ($_COOKIE["affiliateOCG"] != $_SESSION['hash']) {
					$afiliado->__set('saldo', $_SESSION['total'] * 8);
					$afiliado->adicionaSaldoAfiliado();
				}
			}
		}
	}

	
	public function getPublicKey()
	{
		$conta = Container::getModel('ContaMP');
		$key = $conta->getPublicKey();

		if ($key != '' && $key != NULL) {
			if ($key['em_producao'] == 0) {
				return $key['public_key_teste'];
			} else {
				return $key['public_key_producao'];
			}
		} else {
			$conta->__set('idMp', 1);
			$key = $conta->getMpById();
			if ($key['em_producao'] == 0) {
				return $key['public_key_teste'];
			} else {
				return $key['public_key_producao'];
			}
		}
	}

	public function getAccessToken()
	{
		$conta = Container::getModel('ContaMP');
		$access = $conta->getAccessToken();
		$accessFinal = array();

		if ($access != '' && $access != NULL) {
			if ($access['em_producao'] == 0) {
				array_push($accessFinal, $access['id_mp'],$access['access_token_teste'],0);
			} else {
				array_push($accessFinal, $access['id_mp'],$access['access_token_producao'], 1);
			}
		} else {
			$conta->__set('idMp', 1);
			$access = $conta->getMpById();
			if ($access['em_producao'] == 0) {
				array_push($accessFinal, 1, $access['access_token_teste'],0);
			} else {
				array_push($accessFinal, 1, $access['access_token_producao'], 1);
			}
		}
		return $accessFinal;
	}


	public function processarPagamento()
	{
		$this->validaAutenticacao();
		$access = $this->getAccessToken();
		//5031 7557 3453 0604
		MercadoPago\SDK::setAccessToken($access[1]);

		// Variables
		$cardNumber = filter_input(INPUT_POST, 'cardNumber', FILTER_DEFAULT);
		$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
		$securityCode = filter_input(INPUT_POST, 'securityCode', FILTER_DEFAULT);
		$cardExpirationMonth = filter_input(INPUT_POST, 'cardExpirationMonth', FILTER_DEFAULT);
		$cardExpirationYear = filter_input(INPUT_POST, 'cardExpirationYear', FILTER_DEFAULT);
		$cardholderName = filter_input(INPUT_POST, 'cardholderName', FILTER_DEFAULT);
		$docType = filter_input(INPUT_POST, 'docType', FILTER_DEFAULT);
		$docNumber = filter_input(INPUT_POST, 'docNumber', FILTER_DEFAULT);
		$installments = filter_input(INPUT_POST, 'installments', FILTER_DEFAULT);
		$amount = number_format((float)$_SESSION['total'], 2, '.', '');
		$description = filter_input(INPUT_POST, 'description', FILTER_DEFAULT);
		$paymentMethodId = filter_input(INPUT_POST, 'paymentMethodId', FILTER_DEFAULT);
		$token = filter_input(INPUT_POST, 'token', FILTER_DEFAULT);


		//Methods
		$payment = new MercadoPago\Payment();
		$payment->transaction_amount = $amount;
		$payment->token = $token;
		$payment->description = $description;
		$payment->installments = $installments;
		$payment->payment_method_id = $paymentMethodId;
		$payment->payer = array(
			"email" => $email
		);

		$payment->save();
		$_SESSION['payment'] = $payment;
		//
		$exception = Container::getModel('Exception');

		$exception->setPayment($_SESSION['payment']);
		$this->view->exception = $exception->verifyTransaction()['message'];

		if ($exception->verifyTransaction()['pagamento'] == 'aprovado') {
			$venda = Container::getModel('Venda');
			$usuario = Container::getModel('Usuario');
			$usuario->__set('id', $_SESSION['id']);
			$usuario->__set('pontos', $_SESSION['total'] * 10);
			$venda->__set('idConta', $this->verificaUltimaVenda($description));
			$venda->__set('idUsuario', $_SESSION['id']);
			$venda->__set('valor', $amount);
			$venda->__set('pendente', 0);
			$venda->__set('origem', 'Cartão');
			$venda->__set('linkBoleto', NULL);
			$venda->__set('idContaMp', $access[0]);
			$venda->__set('expiracaoBoleto', NULL);
			$venda->__set('chaveResgate', NULL);
			if (isset($_COOKIE["affiliateOCG"]) && $_COOKIE["affiliateOCG"] != $_SESSION['hash']) {
				$venda->__set('afiliado', $_COOKIE["affiliateOCG"]);
				setcookie("affiliateOCG", "", time() - 3600);
			}
			$venda->salvar();
			$usuario->adicionaPontos();
			$this->adicionaSaldoAfiliado();
			$_SESSION['pontos'] += ($_SESSION['total'] * 10);
			unset($_SESSION['boleto']);
			unset($_SESSION['desconto']);
		}
		if ($exception->verifyTransaction()['pagamento'] == 'pendente') {
			$venda = Container::getModel('Venda');
			$venda->__set('idConta', $this->verificaUltimaVenda($description));
			$venda->__set('idUsuario', $_SESSION['id']);
			$venda->__set('valor', $amount);
			$venda->__set('pendente', 1);
			$venda->__set('origem', 'Cartão');
			$venda->__set('idContaMp', $access[0]);
			if (isset($_COOKIE["affiliateOCG"]) && $_COOKIE["affiliateOCG"] != $_SESSION['hash']) {
				$venda->__set('afiliado', $_COOKIE["affiliateOCG"]);
				setcookie("affiliateOCG", "", time() - 3600);
			}
			$venda->salvar();
			unset($_SESSION['boleto']);
			unset($_SESSION['desconto']);
		}
		unset($_SESSION['payment']);
		unset($_SESSION['boleto']);

		$this->render('result');
	}

	public function processarPagamentoBoleto()
	{
		session_start();
		$this->verificaBoletoPendente();
		$access = $this->getAccessToken();
		MercadoPago\SDK::setAccessToken($access[1]);
		$payment = new MercadoPago\Payment();
		$payment->transaction_amount = number_format((float)$_SESSION['total'], 2, '.', '')+ 3;
		$payment->description = $_SESSION['item'];
		$payment->payment_method_id = "bolbradesco";
		$payment->payer = array(
			"email" => $_SESSION['email'],
			"first_name" => $_SESSION['nome'],
			"last_name" => $_SESSION['nome'],
			"identification" => array(
				"type" => "CPF",
				"number" => "19119119100"
			),
			"address" =>  array(
				"zip_code" => "06233200",
				"street_name" => "Av. das Nações Unidas",
				"street_number" => "3003",
				"neighborhood" => "Bonfim",
				"city" => "Osasco",
				"federal_unit" => "SP"
			)
		);

		$payment->save();
		$venda = Container::getModel('Venda');
		$venda->__set('idConta', $this->verificaUltimaVenda($_SESSION['item']));
		$venda->__set('idUsuario', $_SESSION['id']);
		$venda->__set('valor', $_SESSION['total']);
		$venda->__set('pendente', 1);
		$venda->__set('origem', 'Boleto');
		$venda->__set('linkBoleto', $payment->transaction_details->external_resource_url);
		$venda->__set('idContaMp', $access[0]);
		$venda->__set('expiracaoBoleto', $payment->date_of_expiration);
		$_SESSION['boleto'] = $payment->transaction_details->external_resource_url;
		if (isset($_COOKIE["affiliateOCG"]) && $_COOKIE["affiliateOCG"] != $_SESSION['hash']) {
			$venda->__set('afiliado', $_COOKIE["affiliateOCG"]);
			setcookie("affiliateOCG", "", time() - 3600);
		}
		$venda->salvar();
		unset($_SESSION['desconto']);
		$this->render('result');
	}

	public function verificaBoletoPendente()
	{
		$venda = Container::getModel('Venda');
		$venda->__set('idUsuario', $_SESSION['id']);
		if ($venda->verificaBoletoPendente()) {
			echo '<script> location.replace("/meus_jogos?msg=pendente"); </script>';
			die;
		}
	}

	public function verificaNome()
	{
		$this->validaAutenticacao();
		if (isset($_GET['nome'])) {
			$usuario = Container::getModel('Usuario');
			$nick = $_GET['nome'];
			$usuario->__set('nick', $nick);
			$usuario = $usuario->getNick();
			if (count($usuario) > 0 && $nick != $_SESSION['nick']) {
				echo '<small id="info" class="form-text text-danger">' . $nick . ' já está em uso</small>';
			} else {
				echo '<small id="info" class="form-text text-success">' . $nick . ' é um nick válido</small>';
			}
		}
	}

	public function atualizarDados()
	{
		$this->validaAutenticacao();
		$usuario = Container::getModel('Usuario');
		$nome = isset($_POST['name']) ? $_POST['name'] : 'default';
		$nick = isset($_POST['nickname']) ? $_POST['nickname'] : 'default';
		if ($nome != 'default') {
			$usuario = Container::getModel('Usuario');
			$usuario->__set('nome', $nome);
			$usuario->__set('id', $_SESSION['id']);
			$usuario->mudaNome();
			$_SESSION['nome'] = $nome;
		}
		if ($nick != 'default') {
			$usuario = Container::getModel('Usuario');
			$usuario->__set('nick', $nick);
			$usu = $usuario->getNick();
			if (count($usu) < 1 || $nick == $_SESSION['nick']) {
				$usuario->__set('id', $_SESSION['id']);
				$usuario->mudaNick();
				$_SESSION['nick'] = $nick;
			} else {
				echo 'não é pra fazer';
			}
		}

		if (isset($_FILES['fileToUpload']['tmp_name']) && $_FILES['fileToUpload']['tmp_name'] != '') {
			if ($_FILES['fileToUpload']['error'] !== UPLOAD_ERR_OK)
				//echo '<script> location.replace("/meu_perfil"); </script>';
				die("Upload failed with error " . $_FILES['fileToUpload']['error']);

			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($finfo, $_FILES['fileToUpload']['tmp_name']);
			$caminho = "img/usr/" . $_SESSION['id'] . '/';
			switch ($mime) {
				case 'image/jpeg':
					break;
				case 'image/gif':
					break;
				case 'image/png':
					break;
				default:
					echo '<script> location.replace("/meu_perfil?comprovante=erro"); </script>';
					die("Erro / Tipo de arquivo não suportado");
					break;
			}
			if (!is_dir("img/usr/")) {
				mkdir("img/usr/");
			}
			if (!is_dir("img/usr/" . $_SESSION['id'] . '/')) {
				mkdir("img/usr/" . $_SESSION['id'] . '/');
			} else {
				$arquivos = array_diff(scandir($caminho), array('.', '..'));
				foreach ($arquivos as $arquivo) {
					unlink($caminho . $arquivo);
				}
			}
			$extensao = explode('.', $_FILES["fileToUpload"]["name"]);

			$nomeFoto = str_replace('=', '', base64_encode(date('d/m/Y')));
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $caminho . $nomeFoto . '.' . end($extensao))) {
				$usuario->__set('id', $_SESSION['id']);
				$usuario->__set('foto', $caminho . $nomeFoto . '.' . end($extensao));
				$usuario->mudaFoto();
				$_SESSION['foto'] = $caminho . $nomeFoto . '.' . end($extensao);
				echo '<script> location.replace("/meu_perfil?comprovante=success"); </script>';
			} else {
				echo '<script> location.replace("/meu_perfil?comprovante=erro"); </script>';
			}
		}
		echo '<script> location.replace("/meu_perfil?comprovante=success"); </script>';
	}

	public function enviarComprovante()
	{
		$this->validaAutenticacao();
		$nomeJogo = isset($_POST['nome_jogo']) ? $_POST['nome_jogo'] : 'default';
		$idVenda = isset($_POST['id']) ? $_POST['id'] : 'Undefined';
		if ($_FILES['fileToUpload']['error'] !== UPLOAD_ERR_OK)
			die("Upload failed with error " . $_FILES['fileToUpload']['error']);

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $_FILES['fileToUpload']['tmp_name']);
		switch ($mime) {
			case 'image/jpeg':
				break;
			case 'application/pdf':
				break;
			case 'image/png':
				break;
			default:
				echo '<script> location.replace("/meus_jogos?comprovante=erro"); </script>';
				die("Erro / Tipo de arquivo não suportado");
				break;
		}
		if (!is_dir("../comprovantes/")) {
			mkdir("../comprovantes/");
		}
		if (!is_dir("../comprovantes/" . $_SESSION['email'] . '/')) {
			mkdir("../comprovantes/" . $_SESSION['email'] . '/');
		}
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../comprovantes/" . $_SESSION['email'] . '/' . $nomeJogo . ' - ' . $idVenda . ' - ' . $_FILES["fileToUpload"]["name"])) {
			$protocol = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
			$link = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/aprovar_pagamanto_boleto?id_venda='.$idVenda.'&id_usuario='.$_SESSION['id'].'&key=705b10540f48d522a485a3600a6937dc';

			$mensagem = '
				'.$_SESSION['nome'].', de ID: '.$_SESSION['id'].' enviou um comprovante. Verifique em anexo. <br>
				Clique no link para aprovar. <br>
				<a href="'.$link.'" class="btn_aprovar_comprovante">Aprovar</a><br>
			';
			$email = Container::getModel('Email');
			$imagem = "../comprovantes/" . $_SESSION['email'] . '/' . $nomeJogo . ' - ' . $idVenda . ' - ' . $_FILES["fileToUpload"]["name"];
			$email->enviarEmail('Support Onclick','support@onclickgames.com', $_SESSION['nome'].' enviou um comprovante', $mensagem, 1,'erichmartter@gmail.com','','',$imagem);
			echo 'Enviado com sucesso';
		} else {
			echo 'Erro ao enviar';
		}
	}

	public function obterCodigo()
	{
		$this->validaAutenticacao();
		$vendaCod = Container::getModel('Venda');
		$credenciais = Container::getModel('Venda');
		$vendaCod->__set('idUsuario', $_SESSION['id']);
		if (isset($_GET['code'])) {
			$vendaCod->__set('idVenda', $_GET['code']);
			$credenciais->__set('idVenda', $_GET['code']);
			$vendaCod = $vendaCod->getUsuarioVenda();
			$credenciais = $credenciais->getCredenciais();

			if ($vendaCod['limite_cod'] >= 1) {
				$temCodigo = $this->imap($credenciais['email_conta'], $credenciais['pass']);
				if ($temCodigo) {
					$this->subtraiCodigo($_GET['code'], $vendaCod['limite_cod'], $temCodigo, $vendaCod['ultimo_cod']);
				}
			} else {
				echo "Não tem mais códigos";
			}
		} else {
			echo '<script> location.replace("/meus_jogos"); </script>';
		}
	}

	public function verificaUltimaVenda($description)
	{
		$description = $description;
		$venda = Container::getModel('Venda');
		$conta = Container::getModel('Conta');
		$game = Container::getModel('Jogo');
		$game->__set('nomeJogo', str_replace('-', ' ', $description));
		$game = $game->getJogoById();
		$conta->__set('fkContaIdJogo', $game['id_jogo']);
		$venda->__set('idJogo', $game['id_jogo']);
		$ultimaVenda = $venda->getUltimaVenda();

		if (empty($ultimaVenda)) {
			$ultimaVenda = $conta->getAllContasJogo();
			return $ultimaVenda[0]['id_conta'];
		}

		$contas = $conta->getAllContasJogo();
		$arr = array();
		$next = '';
		foreach ($contas as $conta) {
			array_push($arr, $conta['id_conta']);
		}

		for ($i = 0; $i < count($arr); $i++) {
			if ($arr[$i] == $ultimaVenda['id_conta']) {
				if ($arr[$i] == end($arr)) {
					$next =  reset($arr);
				} else {
					$next =  $arr[$i + 1];
				}
			}
		}
		return $next;
	}


	public function imap($emailConta, $passConta)
	{
		date_default_timezone_set('America/Sao_Paulo');
		$hoje = date("d-M-Y");
		if (!$this->verificaImap()) {

			/* Conectando Gmail com Imap */
			$connection = imap_open('{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX', $emailConta, $passConta) or die('Houve um problema! ');

			/* Procurando por emails com o assunto e palavra chave */
			//$emailData = imap_search($connection, 'SUBJECT "Sua conta Steam" ON "'.$hoje.'"');
			//Formato da data: 09-Sep-2020
			$emailData = imap_search($connection, 'SUBJECT "Steam" ON "'.$hoje.'"');
			$temCodigo = false;
			$ipCliente = $_SERVER['REMOTE_ADDR'];
			$steamGuard ='';

			$arr = array();
			if (!empty($emailData)) {
				foreach ($emailData as $emailIdent) {

					$message = imap_fetchbody($connection, $emailIdent, 1);

					if (preg_match('/\s\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s/', $message, $match)) {
						$ipEmail = preg_replace('/[ -]+/' , '' , $match[0]);
					}else{
						$ipEmail = null;
					} 

					if (preg_match('/\s[A-Z0-9]{5}\s/', $message, $matchCod)) {
						$cod = $matchCod[0];
					}else{
						$cod = null;
					} 

					if ($ipCliente == $ipEmail) {
						//Foi identificado que o email que traz o código de quando se pede redefinição de senha vem com o email por extenso. Esse if impede de ele ser inserido no array
						if (strpos($message, $emailConta) === false) {
							array_push($arr, $cod);
							$temCodigo = true;
						}
					}
				} //End foreach
				$steamGuard = end($arr);
				echo $steamGuard;
			} //End if
			if ($temCodigo == false) {
				echo "Nada ainda!";
			}
			imap_close($connection);
			return $steamGuard;
		}
	}

	public function verificarEmail()
	{
		session_start();
		if (!isset($_SESSION['id']) || $_SESSION['id'] == '' || !isset($_SESSION['nome']) || $_SESSION['nome'] == '') {
			header('Location: /?login=erro');
		}
		if (isset($_SESSION['id']) && $_SESSION['nivel'] == 1 && $_SESSION['ativo'] == 1) {
			header('Location: /?verificado=jaVerificado');
		} else {
			$this->render('verificarEmail');
			$email = Container::getModel('Email');
			$modelo = Container::getModel('ModeloHTML');
			$modelo->__set('idModelo', 1);
			$modelo = $modelo->getModeloById()['modelo_html'];
			$protocol = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
			$link = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/ativarConta?key=' . base64_encode($_SESSION['email']) . '&user=' . $_SESSION['hash'] . '&active=1';

			//Preencher isso aqui
			$email->__set('nomeDestinatario', $_SESSION['nome']);
			$email->__set('emailDestinatario', $_SESSION['email']);
			$email->__set('nomeRemetente',  'OnClick Games');
			$email->__set('emailRemetente', 'support@onclickgames.com');
			$email->__set('assunto', 'Verificação de Conta OnClick');
			$modelo = str_replace('nome_do_usuario', $_SESSION['nome'], $modelo);
			$modelo = str_replace('link_da_ativacao', $link, $modelo);
			$email->__set('mensagem', $modelo);

			if (isset($_GET['sent'])) {
				$email->salvarMsgBd();
				$email->enviarEmailConfirmacao($email);
			}

			if (!isset($_SESSION['sent'])) {
				$email->salvarMsgBd();
				$email->enviarEmailConfirmacao($email);
				$_SESSION['sent'] = true;
			}
		}
	}

	public function enviarEmail()
	{

		$email = Container::getModel('Email');
		$smtp = Container::getModel('ContaSMTP');
		$modelo = Container::getModel('ModeloHTML');


		$smtp = $smtp->getAllSMTP();

		$data['conta'] = $smtp->getAllSMTP();
		$data['modelo'] = $modelo->getAllModelo();
		print_r($smtp);
		echo $smtp['email_smtp'];
	}

	public function ativarConta()
	{
		session_start();
		$usuario = Container::getModel('Usuario');
		$hash = isset($_GET['user']) ? $_GET['user'] : 'NULL';
		$email = isset($_GET['key']) ? base64_decode($_GET['key']) : 'NULL';
		$usuario->__set('email', $email);
		$usuario->__set('hash', $hash);

		if ($usuario->getUsuarioPorEmailEHash()) {
			$usuario->__set('id', $usuario->getUsuarioPorEmailEHash()['id']);
			$usuario->ativarConta();
			$_SESSION['nivel'] = 1;
			if (isset($_SESSION['id']) && $_SESSION['id'] != '') {
				header('Location: /');
			} else {
				header('Location: /entrar?active=success');
			}
		} else {
			header('Location: /verificarEmail');
		}
	}

	public function contaDesativada()
	{
		$this->render('contaDesativada');
	}

	public function validaAutenticacao()
	{
		session_start();

		if (!isset($_SESSION['id']) || $_SESSION['id'] == '' || !isset($_SESSION['nome']) || $_SESSION['nome'] == '') {
			header('Location: /?login=erro');
		}

		if (isset($_SESSION['id']) && $_SESSION['nivel'] == 0 && $_SESSION['ativo'] == 1) {
			header('Location: /verificarEmail');
		}


		if (isset($_SESSION['id']) && $_SESSION['ativo'] == 0) {
			session_destroy();
			header('Location: /contaDesativada');
		}
	}
}
