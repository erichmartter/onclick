<?php

namespace App\Controllers;

//os recursos do miniframework
use MF\Controller\Action;
use MF\Model\Container;

class AuthController extends Action {


	public function autenticar() {
		
		$usuario = Container::getModel('Usuario');

		$usuario->__set('email', $_POST['email']);
		$usuario->__set('senha', md5($_POST['senha']));

		$usuario->verificarCredenciais();

		if($usuario->__get('id') != '' && $usuario->__get('nome')) {

			if (isset($_POST['g-recaptcha-response'])) {
				 
				$response =	json_decode($this->validateCaptcha()); 
				
				if($response->success == true && $response->score > 0.4){
					session_start();

					$_SESSION['id'] = $usuario->__get('id');
					$_SESSION['nome'] = $usuario->__get('nome');
					$_SESSION['nivel'] = $usuario->__get('nivel');
					$_SESSION['email'] = $usuario->__get('email');
					$_SESSION['ativo'] = $usuario->__get('ativo');
					$_SESSION['hash'] = $usuario->__get('hash');
					$_SESSION['saldo'] = $usuario->__get('saldo');
					$_SESSION['pontos'] = $usuario->__get('pontos');
					$_SESSION['foto'] = $usuario->__get('foto');
					$_SESSION['nick'] = $usuario->__get('nick');
	   
					header('Location: /meus_jogos');
				}else{
					header('Location: /entrar?login=recaptchaScore');
				}
			}else {
				header('Location: /entrar?login=recaptcha');
			}
		} else {
			header('Location: /entrar?login=erro');
		}

	}

	public function validateCaptcha( $score=0.1){
		if(isset($_POST['g-recaptcha-response']))
		$return = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfmIvYUAAAAAJRVGGE8VIZpbdqe4n0CdYC3jn0c&response=".$_POST['g-recaptcha-response']);
		return $return;
	}
	
	
	public function sair() {
		session_start();
		session_destroy();
		header('Location: /');
	}
}