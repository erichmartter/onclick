<?php

namespace App\Controllers;

//os recursos do miniframework

use MF\Controller\Action;
use MF\Model\Container;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class IndexController extends Action
{

	public function index()
	{
		session_start();
		$this->getJogosDestaques();
		$this->getJogosNovos();
		$this->getJogosCommon();
		$this->render('index');
	}

	public function endpointTeste()
	{
		echo 'Chegou no Endpoint Teste'.
		$_SERVER['REMOTE_ADDR'];
	}

	public function land()
	{
		$this->render('land','layout_land');
	}

	public function sobre()
	{
		session_start();
		$this->render('sobre');
	}

	public function contato()
	{
		session_start();
		$this->render('contato');
	}

	public function regulamento()
	{
		session_start();
		$this->render('regulamento');
	}

	public function loja()
	{
		session_start();
		$search = isset($_GET['search']) ? $_GET['search'] : '';
		$games = array();
		if ($search != '') {
			$game = Container::getModel('Jogo');
			$game->__set('nomeJogo', $search);
			$games = $game->getAllSearch();
			$this->view->games = $games;
			if (count($games) <= 0) {
				$this->view->result = 'empty';
			}
		} else {
			$game = Container::getModel('Jogo');
			$games = $game->getAll();
			$this->view->games = $games;
		}
		$this->render('loja');
	}

	public function enviarEmailContato()
	{
		$email = Container::getModel('Email');

		$email->__set('emailDestinatario', 'support@onclickgames.com');
		$email->__set('emailRemetente', $_POST['email']);
		$email->__set('nomeRemetente', $_POST['nome']);
		$email->__set('assunto', $_POST['assunto']);
		$email->__set('mensagem', $_POST['mensagem']);

		if (!$email->mensagemValida()) {
			echo 'Mensagem de email não é válida';
			header('Location: /contato?msg=erro');
			die();
		}


		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = false;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.hostinger.com.br';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'support@onclickgames.com';                     // SMTP username
			$mail->Password   = 'rpt1g9iS';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom('support@onclickgames.com', $email->nomeRemetente . ' - ' . $email->emailRemetente);
			$mail->addAddress('support@onclickgames.com', 'Support OnClick - ' . $email->assunto);     // Add a recipient
			$mail->addReplyTo($email->emailRemetente, $email->nomeRemetente);
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');

			// Attachments
			//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->CharSet = 'UTF-8';
			$mail->Subject = 'Support OnClick - ' . $email->assunto;
			$mail->Body    = $email->mensagem;
			$mail->AltBody = $email->mensagem;;

			$mail->send();
			echo '<script> location.replace("/contato?msg=success"); </script>';
		} catch (Exception $e) {
			//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			echo '<script> location.replace("/contato?msg=erro"); </script>';
		}
	}

	public function produto()
	{
		session_start();
		if (!isset($_GET['item']) || $_GET['item'] == '') {
			header('Location: /loja');
		} else {
			$produto = $_GET['item'];
		}
		$games = array();
		$game = Container::getModel('Jogo');
		$game->__set('nomeJogo', str_replace('-', ' ', $produto));
		$games = $game->getAllSearch();
		if (!isset($games[0])) {
			header('Location: /loja');
		}
		$this->view->games = $games;
		unset($_SESSION['total']);
		unset($_SESSION['item']);
		$_SESSION['total'] = $games[0]['valor'];
		$_SESSION['item'] = str_replace('-', ' ', $produto);
		if (isset($_GET['affiliate'])) {
			$cookie_value = $_GET['affiliate'];
			setcookie("affiliateOCG", $cookie_value, time() + 86400 * 180, "/"); // 86400 = 1 day
		}
		$this->render('produto');
	}

	public function inscreverse()
	{

		$this->view->usuario = array(
			'nome' => '',
			'email' => '',
			'senha' => '',
		);

		$this->view->signin = false;
		$this->view->erroCadastro = false;

		$this->render('inscreverse');
	}

	public function entrar()
	{
		session_start();
		if (!isset($_SESSION['id'])) {
			$this->view->login = isset($_GET['login']) ? $_GET['login'] : '';
			$this->render('entrar');
		} else {
			$this->view->login = isset($_GET['login']) ? $_GET['login'] : '';
			header('Location: /?login=logado');
		}
	}

	public function atualizarSenha()
	{
		if (isset($_POST['senha'])) {
			if (isset($_POST['g-recaptcha-response'])) {
				$response =	json_decode($this->validateCaptcha());
				if ($response->success == true && $response->score > 0.4) {
					$usuario = Container::getModel('Usuario');
					$usuCodigo = Container::getModel('Usuario');
					$usuCodigo->__set('codigoEmail', $_POST['cod']);
					$usuario->__set('email', base64_decode($_POST['cod']));
					$usuario->__set('senha', md5($_POST['senha']));
					$usuario->mudarSenha();
					$usuCodigo->deletarCodigoEmail();
					echo '<script> location.replace("/entrar?msg=successPass"); </script>';
				} else {
					echo '<script> location.replace("/recuperarSenha?msg=recaptcha"); </script>';
				}
			}
		} else {
			echo '<script> location.replace("/recuperarSenha?msg=recaptcha"); </script>';
		}
	}

	public function recuperarSenha()
	{
		date_default_timezone_set("America/Sao_Paulo");
		$usuario = Container::getModel('Usuario');
		session_start();
		$usuCodigo = Container::getModel('Usuario');
		if (isset($_GET['cod'])) {

			$usuCodigo->__set('codigoEmail', $_GET['cod']);
			if (count($usuCodigo->verificarCodigoEmail()) > 0) {
				$this->render('novaSenha');
			} else {
				echo '<script> location.replace("/recuperarSenha?msg=invalido"); </script>';
			}
		} else {
			$this->render('recuperarSenha');
			$usu = isset($_POST['email']) ? $_POST['email'] : '';
			$usuario->__set('email', $usu);
			if (isset($_POST['g-recaptcha-response'])) {
				$response =	json_decode($this->validateCaptcha());
				if ($response->success == true && $response->score > 0.4) {
					if ($usu != '' && count($usuario->getUsuarioPorEmail()) > 0) {
						$usuario = $usuario->getUsuarioPorEmail();
						$emailCodigo = base64_encode($usu);
						$dataExpirar = date('Y-m-d H:i:s', strtotime('+1 day'));
						$usuCodigo->__set('codigoEmail', $emailCodigo);
						$usuCodigo->__set('dataExpirarcodigoEmail', $dataExpirar);
						$usuCodigo->gravaCodigoEmail();
						$email = Container::getModel('Email');
						$modelo = Container::getModel('ModeloHTML');
						$modelo->__set('idModelo', 2);
						$modelo = $modelo->getModeloById()['modelo_html'];
						$protocol = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
						$link = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/recuperarSenha?cod=' . $emailCodigo;
						$modelo = str_replace('nome_do_usuario', $usuario[0]['nome'], $modelo);
						$modelo = str_replace('link_da_ativacao', $link, $modelo);
						$email->__set('mensagem', $modelo);
						//Preencher isso aqui
						$email->__set('nomeDestinatario', $usuario[0]['nome']);
						$email->__set('emailDestinatario', $usuario[0]['email']);
						$email->__set('nomeRemetente',  'OnClick Games');
						$email->__set('emailRemetente', 'support@onclickgames.com');
						$email->__set('assunto', 'Recuperar senha - OnClick');
						$email->salvarMsgBd();
						$email->enviarEmailRecuperacao($email);
						echo '<script> location.replace("/recuperarSenha?msg=success"); </script>';
					} else {
						echo '<script> location.replace("/recuperarSenha?msg=inexistente"); </script>';
					}
				} else {
					echo '<script> location.replace("/recuperarSenha?msg=recaptcha"); </script>';
				}
			}
		}
	}

	public function registrar()
	{

		$usuario = Container::getModel('Usuario');
		$dominio = Container::getModel('Usuario');

		$usuario->__set('hash', $usuario->randomString());
		while (count($usuario->verificaHash()) > 0) {
			$usuario->__set('hash', $usuario->randomString());
		}

		$_SESSION['count'] = count($usuario->verificaHash());
		$usuario->__set('nome', $_POST['nome']);
		$usuario->__set('email', $_POST['email']);
		$usuario->__set('senha', md5($_POST['senha']));

		$email = explode('@', $_POST['email']);
		$dom = $email[1];
		$email = $dominio->getDominio($dom);



		if ($usuario->validarCadastro() && count($usuario->getUsuarioPorEmail()) == 0) {
			if (isset($_POST['g-recaptcha-response'])) {

				$response =	json_decode($this->validateCaptcha());
				if ($response->success == true && $response->score > 0.4) {
					if (count($email) > 0) {
						$usuario->salvar();
						$usuario->verificarCredenciais();
						session_start();

						$_SESSION['id'] = $usuario->__get('id');
						$_SESSION['nome'] = $usuario->__get('nome');
						$_SESSION['nivel'] = $usuario->__get('nivel');
						$_SESSION['email'] = $usuario->__get('email');
						$_SESSION['ativo'] = $usuario->__get('ativo');
						$_SESSION['hash'] = $usuario->__get('hash');
						$_SESSION['saldo'] = $usuario->__get('saldo');
						$_SESSION['pontos'] = $usuario->__get('pontos');
						$_SESSION['foto'] = $usuario->__get('foto');

						header('Location: /verificarEmail');
					} else {
						$this->view->usuario = array(
							'nome' => $_POST['nome'],
							'email' => $_POST['email'],
							'senha' => $_POST['senha'],
						);
						$this->view->signin = "email";
						$this->render('inscreverse');
					}
				} else {
					$this->view->usuario = array(
						'nome' => $_POST['nome'],
						'email' => $_POST['email'],
						'senha' => $_POST['senha'],
					);
					$this->view->signin = "recaptcha";
					$this->render('inscreverse');
				}
			} else {
				$this->view->usuario = array(
					'nome' => $_POST['nome'],
					'email' => $_POST['email'],
					'senha' => $_POST['senha'],
				);
				header('Location: /inscreverse?signin=semDadosRecaptcha');
			}
		} else {
			$this->view->usuario = array(
				'nome' => $_POST['nome'],
				'email' => $_POST['email'],
				'senha' => $_POST['senha'],
			);
			$this->view->erroCadastro = "erro";
			$this->view->signin = false;
			$this->render('inscreverse');
		}
	}

	public function validateCaptcha($score = 0.1)
	{
		if (isset($_POST['g-recaptcha-response']))
			$return = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfmIvYUAAAAAJRVGGE8VIZpbdqe4n0CdYC3jn0c&response=" . $_POST['g-recaptcha-response']);
		return $return;
	}

	public function getJogosDestaques()
	{
		$jogo = Container::getModel('Jogo');

		$jogosD = $jogo->getAllIndexDestaques();
		$this->view->jogosD = $jogosD;
	}

	public function getJogosNovos()
	{
		$jogo = Container::getModel('Jogo');

		$jogosN = $jogo->getAllIndexNovos();
		$this->view->jogosN = $jogosN;
	}

	public function getJogosCommon()
	{
		$jogo = Container::getModel('Jogo');

		$jogosC = $jogo->getAllIndexCommon();
		$this->view->jogosC = $jogosC;
	}
}
