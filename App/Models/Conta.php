<?php

namespace App\Models;

use MF\Model\Model;

class Conta extends Model {
	private $id;
	private $usuarioConta;
	private $senhaConta;
	private $fkContaIdJogo;
	private $emailConta;
	private $pass;
	private $ativa;

	public function __get($atributo) {
		return $this->$atributo;
	}

	public function __set($atributo, $valor) {
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar() {
		$query = "insert into jogo(nome_jogo, desc_jogo)values(:nomeJogo, :descJogo)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', $this->__get('nomeJogo'));
		$stmt->bindValue(':descJogo', $this->__get('descJogo'));
		$stmt->execute();
		return $this;
	}

	//recuperar
	public function getAllContasJogo() {

		$query = "
		select * from conta
		WHERE fk_conta_id_jogo = :fkContaIdJogo
		AND
		ativa = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':fkContaIdJogo', $this->__get('fkContaIdJogo'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}