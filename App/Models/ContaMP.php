<?php

namespace App\Models;

use MF\Model\Model;

class ContaMP extends Model
{
	private $idMp ;
	private $descMP;
	private $publicKeyProducao;
	private $accessTokenProducao;
	private $publicKeyTeste;
	private $accessTokenTeste;

	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into smtp(desc_mp, public_key_producao, access_token_producao, public_key_teste, access_token_teste)values(:descMP, :publicKeyProducao, :accessTokenProducao, :publicKeyTeste, :accessTokenTeste)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':descMP', $this->__get('descMP'));
		$stmt->bindValue(':publicKeyProducao', $this->__get('publicKeyProducao'));
		$stmt->bindValue(':accessTokenProducao', $this->__get('accessTokenProducao'));
		$stmt->bindValue(':publicKeyTeste', $this->__get('publicKeyTeste'));
		$stmt->bindValue(':accessTokenTeste', $this->__get('accessTokenTeste'));
		$stmt->execute();

		return $this;
	}

	public function getMpById()
	{
		$query = "select * from contas_mp where id_mp = :idMp";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idMp', $this->__get('idMp'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function getPublicKey()
	{
		$query = "select public_key_producao, public_key_teste, em_producao from contas_mp where atual = 1";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	public function getAccessToken()
	{
		$query = "select id_mp, access_token_producao, access_token_teste, em_producao from contas_mp where atual = 1";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function getContaMPAual()
	{
		$query = "select * from contas_mp where atual = 1";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}


	
	public function getAllMp()
	{
		$query = "select * from contas_mp";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}


}
