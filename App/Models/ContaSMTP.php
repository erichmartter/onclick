<?php

namespace App\Models;

use MF\Model\Model;

class ContaSMTP extends Model
{
	private $idSmtp;
	private $nomeSmtp;
	private $descSmtp;
	private $emailSmtp;
	private $senhaSmtp;
	private $hostSmtp;
	private $portaSmtp;

	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into smtp(nome_smtp, desc_smtp, email_smtp, senha_smtp, host_smtp, porta_smtp)values(:nomeSmtp, :descSmtp, :emailSmtp, :senhaSmtp,:hostSmtp, :portaSmtp)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeSmtp', $this->__get('nomeSmtp'));
		$stmt->bindValue(':descSmtp', $this->__get('descSmtp'));
		$stmt->bindValue(':emailSmtp', $this->__get('emailSmtp'));
		$stmt->bindValue(':senhaSmtp', $this->__get('senhaSmtp'));
		$stmt->bindValue(':hostSmtp', $this->__get('hostSmtp'));
		$stmt->bindValue(':portaSmtp', $this->__get('portaSmtp'));
		$stmt->execute();

		return $this;
	}

	public function getSMTPById()
	{
		$query = "select * from smtp where id_smtp = :idSmtp";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idSmtp', $this->__get('idSmtp'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	
	public function getAllSMTP()
	{
		$query = "select * from smtp";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}


}
