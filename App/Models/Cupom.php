<?php

namespace App\Models;

use MF\Model\Model;

class Cupom extends Model
{
	private $id;
	private $ativo;
	private $conteudo;
	private $dataValidade;

	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into tweets(id_usuario, tweet)values(:id_usuario, :tweet)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id_usuario', $this->__get('id_usuario'));
		$stmt->bindValue(':tweet', $this->__get('tweet'));
		$stmt->execute();

		return $this;
	}

	public function getAllActive()
	{
		$query = "
			select * from cupom where ativo = 1 and dataValidade > NOW()";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//recuperar
	public function getCupom()
	{
		$query = "select * from cupom as c
		left join cupom_jogo as cj on (c.id = cj.fk_id_cupom)
		where ativo = 1 and dataValidade > NOW() and conteudo = :conteudo";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':conteudo', $this->__get('conteudo'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}
