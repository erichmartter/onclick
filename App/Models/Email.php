<?php

namespace App\Models;

use MF\Model\Model;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use MF\Model\Container;

class Email extends Model
{
	private $id;
	private $nomeDestinatario;
	private $emailDestinatario;
	private $nomeDestinatarioCopia;
	private $emailDestinatarioCopia;
	private $nomeRemetente;
	private $emailRemetente;
	private $assunto;
	private $mensagem;
	private $endereco;
	private $senha;


	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	public function mensagemValida()
	{
		if (empty($this->emailDestinatario) || empty($this->emailRemetente) || empty($this->mensagem || empty($this->assunto))) {
			return false;
		}
		return true;
	}

	//salvar
	public function salvarMsgBd()
	{	
		$query = "insert into email(nome_destinatario, email_destinatario, nome_destinatario_copia, email_destinatario_copia, nome_remetente, email_remetente, assunto, mensagem)values
		(:nomeDestinatario, :emailDestinatario, :nomeDestinatarioCopia , :emailDestinatarioCopia , :nomeRemetente , :emailRemetente , :assunto , :mensagem)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeDestinatario', $this->__get('nomeDestinatario'));
		$stmt->bindValue(':emailDestinatario', $this->__get('emailDestinatario'));
		$stmt->bindValue(':nomeDestinatarioCopia', $this->__get('nomeDestinatarioCopia'));
		$stmt->bindValue(':emailDestinatarioCopia', $this->__get('emailDestinatarioCopia'));
		$stmt->bindValue(':nomeRemetente', $this->__get('nomeRemetente'));
		$stmt->bindValue(':emailRemetente', $this->__get('emailRemetente'));
		$stmt->bindValue(':assunto', $this->__get('assunto'));
		$stmt->bindValue(':mensagem', $this->__get('mensagem'));
		$stmt->execute();
		return $this;
	}

	public function teste(){
		$this->enviarEmail('Support Onclick','support@onclickgames.com', $_SESSION['nome'].' enviou um comprovante', 'Mensagem de Teste',1, 'erichmartter@gmail.com');
		
	}

	public function enviarEmail($nomeDestinatario ='', $emailDestinatario='', $assunto='', $mensagem='', $contaSMTP = 1, $emailDestinatarioCopia='', $emailReply ='', $nomeReply='', $anexo = ''){
		$nomeDestinatario = $nomeDestinatario;
		$emailDestinatario = $emailDestinatario;
		$emailDestinatarioCopia = $emailDestinatarioCopia;
		$nomeReply = $nomeReply;
		$emailReply = $emailReply;
		$assunto = $assunto;
		$mensagem = $mensagem;
		$idContaSMTP = $contaSMTP;
		$anexo = $anexo;
		
		$smtp = Container::getModel('ContaSMTP');
		$smtp->__set('idSmtp', $idContaSMTP);
		$contaSMTP = $smtp->getSMTPById();

				// Instantiation and passing `true` enables exceptions
				$mail = new PHPMailer(true);

				try {
					//Server settings
					$mail->SMTPDebug = false;                      // Enable verbose debug output
					$mail->isSMTP();                                            // Send using SMTP
					$mail->Host       = $contaSMTP['host_smtp'];                    // Set the SMTP server to send through
					$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					$mail->Username   = $contaSMTP['email_smtp'];                     // SMTP username
					$mail->Password   = $contaSMTP['senha_smtp'];                             // SMTP password
					$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					$mail->Port       = $contaSMTP['porta_smtp'];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
		
					//Recipients
					$mail->setFrom($contaSMTP['email_smtp'], $contaSMTP['nome_smtp']);
					$mail->addAddress($emailDestinatario,  $nomeDestinatario);     // Add a recipient
					
					if($emailReply && $nomeReply != ''){
						$mail->addReplyTo($emailReply, $nomeReply);
					}
					
					if($emailDestinatarioCopia != ''){
						$mail->addBCC($emailDestinatarioCopia);
					}
		
					if($anexo != ''){
						$mail->addAttachment($anexo);
					}

					// Attachments
					//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
					//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		
					// Content
					$mail->isHTML(true);                                  // Set email format to HTML
					$mail->CharSet = 'UTF-8';
					$mail->Subject =  $assunto;
					$mail->Body    = $mensagem;
					$mail->AltBody = $mensagem;
		
					$mail->send();
				} catch (Exception $e) {
					echo "A Mensagem não pôde ser enviada. Erro: {$mail->ErrorInfo} <br>";
				}

	}

	public function enviarEmailConfirmacao($emailSalvo)
	{
		$emailSalvo = $emailSalvo;
		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = false;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.hostinger.com.br';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'support@onclickgames.com';                     // SMTP username
			$mail->Password   = 'rpt1g9iS';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom('support@onclickgames.com', $emailSalvo->nomeRemetente);
			$mail->addAddress($emailSalvo->emailDestinatario,  $emailSalvo->assunto);     // Add a recipient
			//$mail->addReplyTo($emailSalvo->emailRemetente, $emailSalvo->nomeRemetente);
			// //$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');

			// Attachments
			//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->CharSet = 'UTF-8';
			$mail->Subject =  $emailSalvo->assunto;
			$mail->Body    = $emailSalvo->mensagem;
			$mail->AltBody = $emailSalvo->mensagem;

			$mail->send();
		} catch (Exception $e) {
			//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo} <br>";
			$_SESSION['nivel'] = 1;
			echo '<script> location.replace("/"); </script>';
		}
	}

	public function enviarEmailRecuperacao($emailSalvo)
	{
		$emailSalvo = $emailSalvo;
		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = false;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = 'smtp.hostinger.com.br';                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'support@onclickgames.com';                     // SMTP username
			$mail->Password   = 'rpt1g9iS';                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom('support@onclickgames.com', $emailSalvo->nomeRemetente);
			$mail->addAddress($emailSalvo->emailDestinatario,  $emailSalvo->assunto);     // Add a recipient
			//$mail->addReplyTo($emailSalvo->emailRemetente, $emailSalvo->nomeRemetente);
			// //$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');

			// Attachments
			//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->CharSet = 'UTF-8';
			$mail->Subject =  $emailSalvo->assunto;
			$mail->Body    = $emailSalvo->mensagem;
			$mail->AltBody = $emailSalvo->mensagem;

			$mail->send();
		} catch (Exception $e) {
			//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo} <br>";
			echo '<script> location.replace("/recuperarSenha?msg=erro"); </script>';
		}
	}
}
