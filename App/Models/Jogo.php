<?php

namespace App\Models;

use MF\Model\Model;

class Jogo extends Model
{
	private $id;
	private $nomeJogo;
	private $descJogo;
	private $requisitos;
	private $ativo;
	private $data;

	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into jogo(nome_jogo, desc_jogo)values(:nomeJogo, :descJogo)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', $this->__get('nomeJogo'));
		$stmt->bindValue(':descJogo', $this->__get('descJogo'));
		$stmt->execute();

		return $this;
	}

	//recuperar
	public function getAllSearch()
	{

		$query = "
		select * 
		from jogo as j
		left join imagem_item as i on (j.id_jogo = i.id_item)
		left join jogo_genero as jg on (j.id_jogo = jg.id_jogo)
		left join genero as g on (jg.id_genero = g.id_genero)
		where j.nome_jogo like :nomeJogo 
		AND j.ativo = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', '%'.$this->__get('nomeJogo').'%');
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function getJogoByName()
	{

		$query = "
		select j.id_jogo 
		from jogo as j
		where j.nome_jogo = :nomeJogo 
		AND j.ativo = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', $this->__get('nomeJogo'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function getJogoById()
	{

		$query = "
		select j.id_jogo 
		from jogo as j
		where j.nome_jogo like :nomeJogo 
		AND j.ativo = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', $this->__get('nomeJogo'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	
	public function getAll()
	{

		$query = "
		select * 
		from jogo as j
		left join imagem_item as i on (j.id_jogo = i.id_item)
		left join jogo_genero as jg on (j.id_jogo = jg.id_jogo)
		left join genero as g on (jg.id_genero = g.id_genero)
		where j.ativo = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeJogo', '%'.$this->__get('nomeJogo').'%');
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function getAllIndexDestaques()
	{

		$query = "
			select * 
			from jogo as j
			left join imagem_item as i on (j.id_jogo = i.id_item)
			left join jogo_genero as jg on (j.id_jogo = jg.id_jogo)
			left join genero as g on (jg.id_genero = g.id_genero)
			where j.ativo = 1 and j.destaque = 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function getAllIndexNovos()
	{

		$query = "
		select j.id_jogo, j.nome_jogo, j.valor, g.genero, i.path, j.data_cadastro
		from jogo as j
		 left join imagem_item as i on (j.id_jogo = i.id_item)
		 left join jogo_genero as jg on (j.id_jogo = jg.id_jogo)
		 left join genero as g on (jg.id_genero = g.id_genero)
		where j.ativo = 1
		AND
		j.data_cadastro > ADDDATE(CURDATE(), INTERVAL -30 DAY)
		";
		//AND j.data_cadastro > ADDDATE(CURDATE(), INTERVAL -30 DAY)
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function getAllIndexCommon()
	{
		$query = "
			select * 
			from jogo as j
			left join imagem_item as i on (j.id_jogo = i.id_item)
			left join jogo_genero as jg on (j.id_jogo = jg.id_jogo)
			left join genero as g on (jg.id_genero = g.id_genero)
			where j.ativo = 1 AND j.destaque = 0
			LIMIT 6
		";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

}
