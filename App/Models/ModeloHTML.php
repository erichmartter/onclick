<?php

namespace App\Models;

use MF\Model\Model;

class ModeloHTML extends Model
{
	private $idModelo;
	private $descModelo;
	private $modeloHtml;

	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into modelo_html(desc_modelo, modelo_html)values(:descModelo, :modeloHtml)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':descModelo', $this->__get('descModelo'));
		$stmt->bindValue(':modeloHtml', $this->__get('modeloHtml'));
		$stmt->execute();

		return $this;
	}

	public function getModeloById()
	{
		$query = "select * from modelo_html where id_modelo = :idModelo";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idModelo', $this->__get('idModelo'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	
	public function getAllModelo()
	{
		$query = "select * from modelo_html";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}


}
