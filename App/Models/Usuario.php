<?php

namespace App\Models;

use MF\Model\Model;

class Usuario extends Model {

	private $id;
	private $nome;
	private $email;
	private $senha;
	private $nick;
	private $hash;
	private $saldo;
	private $pontos;
	private $codigoEmail;
	private $foto;
	private $dataExpirarcodigoEmail;
	private $nomeDominio;

	public function __get($atributo) {
		return $this->$atributo;
	}

	public function __set($atributo, $valor) {
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar() {

		$query = "insert into usuarios(nome, email, senha, hash)values(:nome, :email, :senha, :hash)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nome', $this->__get('nome'));
		$stmt->bindValue(':email', $this->__get('email'));
		$stmt->bindValue(':senha', $this->__get('senha')); //md5() -> hash 32 caracteres
		$stmt->bindValue(':hash', $this->__get('hash'));
		$stmt->execute();

		return $this;
	}

	public function getAllUsuarios(){
		$query = "select * from usuarios ORDER BY id DESC";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//validar se um cadastro pode ser feito
	public function validarCadastro() {
		$valido = true;

		if(strlen($this->__get('nome')) < 3) {
			$valido = false;
		}

		if(strlen($this->__get('email')) < 3) {
			$valido = false;
		}

		if(strlen($this->__get('senha')) < 3) {
			$valido = false;
		}


		return $valido;
	}

	//recuperar um usuário por e-mail
	public function getUsuarioPorEmail() {
		$query = "select nome, email, hash from usuarios where email = :email";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':email', $this->__get('email'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function getUsuarioPorEmailEHash() {
		$query = "select id, email, hash from usuarios where email = :email and hash = :hash";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':email', $this->__get('email'));
		$stmt->bindValue(':hash', $this->__get('hash'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function getUsuarioPorHash(){
		$query = "select id, hash from usuarios where hash = :hash";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':hash', $this->__get('hash'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function getNick(){
		$query = "select id, nick from usuarios where nick = :nick";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nick', $this->__get('nick'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function mudaNome(){
		$query = "update usuarios SET nome = :nome WHERE id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nome', $this->__get('nome'));
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	public function mudaNick(){
		$query = "update usuarios SET nick = :nick WHERE id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nick', $this->__get('nick'));
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	

	public function adicionaPontos(){
		$query = "UPDATE usuarios
				SET pontos = pontos + :pontos
				WHERE id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':pontos', $this->__get('pontos'));
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function adicionaSaldoAfiliado(){
		$query = "update usuarios SET vendas = vendas + 1, saldo = saldo + :saldo WHERE hash = :hash";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':hash', $this->__get('hash'));
		$stmt->bindValue(':saldo', $this->__get('saldo'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function mudarSenha(){
		$query = "UPDATE usuarios
				SET senha = :senha
				WHERE email = :email";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':email', $this->__get('email'));
		$stmt->bindValue(':senha', $this->__get('senha'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
		
	}

	public function verificarCodigoEmail(){
		$query = "select * from codigos_email where codigo = :codigoEmail and data > NOW()";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':codigoEmail', $this->__get('codigoEmail'));
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function gravaCodigoEmail(){
		$query = "insert into codigos_email(codigo, data)values(:codigoEmail, :dataExpirarcodigoEmail)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':codigoEmail', $this->__get('codigoEmail'));
		$stmt->bindValue(':dataExpirarcodigoEmail', $this->__get('dataExpirarcodigoEmail'));
		$stmt->execute();

		return $this;
	}

	public function deletarCodigoEmail(){
		$query = "DELETE FROM codigos_email WHERE codigo = :codigoEmail";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':codigoEmail', $this->__get('codigoEmail'));
		$stmt->execute();
	}

	public function verificaHash() {
		$query = "select id from usuarios where hash = :hash";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':hash', $this->__get('hash'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function randomString() {
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$palavra = array(); //deve declarar palavra array antes
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < 12; $i++) {// aqui você escolhe o tamanho da string
			$n = rand(0, $alphaLength);
			$palavra [] = $alphabet[$n];
		}
		return implode($palavra); //transforma o array numa string
	}

	public function getDominio($dominio) {
		$query = "select id_dominio from dominio where nome_dominio = :nomeDominio";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nomeDominio', $dominio);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function mudaFoto(){
		$query = "UPDATE usuarios
				 SET foto = :foto
				 WHERE id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':foto', $this->__get('foto'));
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function ativarConta(){
		$query = "UPDATE usuarios
				 SET nivel = 1
				 WHERE id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function verificarCredenciais() {

		$query = "select id, nome, email, nivel, ativo, hash, saldo, pontos, foto, nick from usuarios where email = :email and senha = :senha";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':email', $this->__get('email'));
		$stmt->bindValue(':senha', $this->__get('senha'));
		$stmt->execute();

		$usuario = $stmt->fetch(\PDO::FETCH_ASSOC);

		if($usuario['id'] != '' && $usuario['nome'] != '') {
			$this->__set('id', $usuario['id']);
			$this->__set('nome', $usuario['nome']);
			$this->__set('nivel', $usuario['nivel']);
			$this->__set('ativo', $usuario['ativo']);
			$this->__set('hash', $usuario['hash']);
			$this->__set('saldo', $usuario['saldo']);
			$this->__set('pontos', $usuario['pontos']);
			$this->__set('foto', $usuario['foto']);
			$this->__set('nick', $usuario['nick']);
		}

		return $this;
	}

	public function getAll() {
		$query = "
			select 
				u.id, 
				u.nome, 
				u.email,
				(
					select
						count(*)
					from
						usuarios_seguidores as us 
					where
						us.id_usuario = :id_usuario and us.id_usuario_seguindo = u.id
				) as seguindo_sn
			from  
				usuarios as u
			where 
				u.nome like :nome and u.id != :id_usuario
			";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':nome', '%'.$this->__get('nome').'%');
		$stmt->bindValue(':id_usuario', $this->__get('id'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}


	//Informações do Usuário
	public function getInfoUsuario() {
		$query = "select nome, email, nick, foto, nivel,  ativo from usuarios where id = :id_usuario";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id_usuario', $this->__get('id'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
}
