<?php

namespace App\Models;

use MF\Model\Model;

class Venda extends Model
{

	private $idVenda;
	private $idJogo;
	private $idConta;
	private $item;
	private $idUsuario;
	private $valor;
	private $pendente;
	private $penultimoCod;
	private $ultimoCod;
	private $limiteCod;
	private $dataVenda;
	private $origem;
	private $linkBoleto;
	private $idContaMp;
	private $expiracaoBoleto;
	private $afiliado;
	private $chaveResgate;
	private $dataUltimoCod;
	private $plataforma;


	public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

	//salvar
	public function salvar()
	{
		$query = "insert into venda(id_conta, id_usuario, valor, pendente, origem, link_boleto, id_conta_mp, expiracao_boleto, afiliado, chave_resgate)values(:idConta, :idUsuario, :valor, :pendente, :origem, :linkBoleto, :idContaMp, :expiracaoBoleto, :afiliado, :chaveResgate)";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idConta', $this->__get('idConta'));
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':valor', $this->__get('valor'));
		$stmt->bindValue(':pendente', $this->__get('pendente'));
		$stmt->bindValue(':origem', $this->__get('origem'));
		$stmt->bindValue(':linkBoleto', $this->__get('linkBoleto'));
		$stmt->bindValue(':idContaMp', $this->__get('idContaMp'));
		$stmt->bindValue(':expiracaoBoleto', $this->__get('expiracaoBoleto'));
		$stmt->bindValue(':afiliado', $this->__get('afiliado'));
		$stmt->bindValue(':chaveResgate', $this->__get('chaveResgate'));
		$stmt->execute();

		return $this;
	}

	public function getAllVendas(){
		$query = "select * from venda as v
		left join usuarios as u on (v.id_usuario = u.id)
		left join conta as c on (v.id_conta = c.id_conta)
		left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
		ORDER BY id_venda DESC";
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//Recupera todos os jogos não pendentes do usuário logado
	public function getAllUsuarioVenda()
	{
		$query = "
			select v.id_venda, v.id_conta, v.id_usuario, DATE_FORMAT(v.data_venda, '%d/%m/%Y') as data, v.penultimo_cod, v.ultimo_cod, v.limite_cod, DATE_FORMAT(v.data_ultimo_cod,'%d/%m/%Y %h:%m') as data_ultimo_cod,
			c.usuario_conta, c.senha_conta,
			j.nome_jogo, j.desc_jogo, j.img_icone, j.plataforma
			from 
			venda as v
			left join usuarios as u on (v.id_usuario = u.id)
			left join conta as c on (v.id_conta = c.id_conta)
			left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
			where
			v.id_usuario = :idUsuario
			AND
			v.pendente = 0
			AND
			j.plataforma = :plataforma
			ORDER BY v.data_venda DESC
			";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':plataforma', $this->__get('plataforma'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//Recupera todos os jogos pendentes do usuário logado
	public function getAllUsuarioVendaPendenteBoleto()
	{
		$query = "
				select v.id_venda, v.id_conta, v.id_usuario, DATE_FORMAT(v.data_venda, '%d/%m/%Y') as data, v.expiracao_boleto, link_boleto, v.penultimo_cod, v.ultimo_cod, v.limite_cod, DATE_FORMAT(v.data_ultimo_cod,'%d/%m/%Y %h:%m') as data_ultimo_cod, v.afiliado,
				c.usuario_conta, c.senha_conta,
				j.nome_jogo, j.desc_jogo, j.img_icone, j.plataforma
				from 
				venda as v
				left join usuarios as u on (v.id_usuario = u.id)
				left join conta as c on (v.id_conta = c.id_conta)
				left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
				where
				v.id_usuario = :idUsuario
				AND
				v.pendente = 1
				AND
				j.plataforma = :plataforma
				AND
				v.expiracao_boleto > NOW()
				ORDER BY v.data_venda DESC
				";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':plataforma', $this->__get('plataforma'));
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function verificaBoletoPendente()
	{
		$query = "
				select v.id_venda, v.id_conta, v.id_usuario, DATE_FORMAT(v.data_venda, '%d/%m/%Y') as data, v.expiracao_boleto , v.penultimo_cod, v.ultimo_cod, v.limite_cod, DATE_FORMAT(v.data_ultimo_cod,'%d/%m/%Y %h:%m') as data_ultimo_cod,
				c.usuario_conta, c.senha_conta,
				j.nome_jogo, j.desc_jogo, j.img_icone
				from 
				venda as v
				left join usuarios as u on (v.id_usuario = u.id)
				left join conta as c on (v.id_conta = c.id_conta)
				left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
				where
				v.id_usuario = :idUsuario
				AND
				v.pendente = 1
				AND
				v.expiracao_boleto > NOW()
				AND 
				j.nome_jogo = :item";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':item', $_SESSION['item']);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//Recupera a última conta entregue do jogo em questão
	public function getUltimaVenda()
	{
		$query = "
		select v.id_venda, v.id_conta, v.data_venda,
		c.id_conta
		from 
		venda as v
		left join conta as c on (v.id_conta = c.id_conta)
		left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
		WHERE c.fk_conta_id_jogo = :idJogo
		ORDER BY v.data_venda DESC
		limit 1
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idJogo', $this->__get('idJogo'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	
	//Recupera o registro do jogo para receber o código
	public function getUsuarioVenda()
	{
		$query = "
			select v.id_venda, v.id_conta, v.id_usuario, DATE_FORMAT(v.data_venda, '%d/%m/%Y') as data, v.penultimo_cod, v.ultimo_cod, v.limite_cod, DATE_FORMAT(v.data_ultimo_cod,'%d/%m/%Y %h:%m') as data_ultimo_cod,  v.afiliado, v.valor,
			c.usuario_conta, c.senha_conta,
			j.nome_jogo, j.desc_jogo, j.img_icone
			from 
			venda as v
			left join usuarios as u on (v.id_usuario = u.id)
			left join conta as c on (v.id_conta = c.id_conta)
			left join jogo as j on (c.fk_conta_id_jogo = j.id_jogo)
			where
			v.id_usuario = :idUsuario
			AND
			v.id_venda = :idVenda
			";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':idVenda', $this->__get('idVenda'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
	//Executa a lógia de atualizar o número de códigos restantes do usuário
	public function subtraiCodigoSalvar()
	{
		$query = "UPDATE venda 
		SET limite_cod = :limiteCod, ultimo_cod = :ultimoCod, penultimo_cod = :penultimoCod, data_ultimo_cod = :dataUltimoCod
					WHERE 
					id_venda = :idVenda
					AND
					id_usuario = :idUsuario";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idVenda', $this->__get('idVenda'));
		$stmt->bindValue(':ultimoCod', $this->__get('ultimoCod'));
		$stmt->bindValue(':penultimoCod', $this->__get('penultimoCod'));
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->bindValue(':limiteCod', $this->__get('limiteCod'));
		$stmt->bindValue(':dataUltimoCod', $this->__get('dataUltimoCod'));
		$stmt->execute();
	}

	public function getCredenciais()
	{
		$query = "
		select v.id_venda, v.id_conta, c.email_conta, c.pass
		from 
		venda as v
		left join conta as c on (v.id_conta = c.id_conta)
		where
		v.id_venda = :idVenda
		";

		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idVenda', $this->__get('idVenda'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function ativaVenda()
	{
		$query = "update venda SET pendente = 0 WHERE id_usuario = :idUsuario AND id_venda = :idVenda";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':idVenda', $this->__get('idVenda'));
		$stmt->bindValue(':idUsuario', $this->__get('idUsuario'));
		$stmt->execute();

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public function insereCodigoInicioMes()
	{
		$query = 'UPDATE `venda` SET `limite_cod`= 3';
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	}
}
