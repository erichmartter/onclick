<?php

namespace App;

use MF\Init\Bootstrap;

class Route extends Bootstrap {

	protected function initRoutes() {
		//AdmController

		$routes['oi'] = array(
			'route' => '/oi',
			'controller' => 'AdmController',
			'action' => 'oi'
		);

		$routes['admin'] = array(
			'route' => '/admin',
			'controller' => 'AdmController',
			'action' => 'admin'
		);
		
		$routes['salvar_jogo'] = array(
			'route' => '/salvar_jogo',
			'controller' => 'AdmController',
			'action' => 'salvarJogo'
		);

		$routes['adm_jogos'] = array(
			'route' => '/adm_jogos',
			'controller' => 'AdmController',
			'action' => 'admJogos'
		);
		$routes['adm_vendas'] = array(
			'route' => '/adm_vendas',
			'controller' => 'AdmController',
			'action' => 'admVendas'
		);

		$routes['aprovar_comprovante'] = array(
			'route' => '/aprovar_pagamanto_boleto',
			'controller' => 'AdmController',
			'action' => 'aprovarPagamantoBoleto'
		);

		//AppController

		$routes['pagamento'] = array(
			'route' => '/pagamento',
			'controller' => 'AppController',
			'action' => 'pagamento'
		);

		$routes['verificaCupom'] = array(
			'route' => '/verificaCupom',
			'controller' => 'AppController',
			'action' => 'verificaCupom'
		);

		$routes['enviarComprovante'] = array(
			'route' => '/enviarComprovante',
			'controller' => 'AppController',
			'action' => 'enviarComprovante'
		);

		$routes['result'] = array(
			'route' => '/result',
			'controller' => 'AppController',
			'action' => 'result'
		);

		$routes['processar_pagamento'] = array(
			'route' => '/processar_pagamento',
			'controller' => 'AppController',
			'action' => 'processarPagamento'
		);
		$routes['processar_pagamento_boleto'] = array(
			'route' => '/processar_pagamento_boleto',
			'controller' => 'AppController',
			'action' => 'processarPagamentoBoleto'
		);

		$routes['busca_codigo'] = array(
			'route' => '/busca_codigo',
			'controller' => 'AppController',
			'action' => 'busca_codigo'
		);

		$routes['salva_codigo'] = array(
			'route' => '/salva_codigo',
			'controller' => 'AppController',
			'action' => 'salvaCodigo'
		);

		$routes['venda'] = array(
			'route' => '/venda',
			'controller' => 'AppController',
			'action' => 'cod'
		);
		
		$routes['code'] = array(
			'route' => '/code',
			'controller' => 'AppController',
			'action' => 'code'
		);

		$routes['obter_codigo'] = array(
			'route' => '/obter_codigo',
			'controller' => 'AppController',
			'action' => 'obterCodigo'
		);

		$routes['meus_jogos'] = array(
			'route' => '/meus_jogos',
			'controller' => 'AppController',
			'action' => 'meusJogos'
		);

		$routes['meu_perfil'] = array(
			'route' => '/meu_perfil',
			'controller' => 'AppController',
			'action' => 'meuPerfil'
		);

		$routes['tweet'] = array(
			'route' => '/tweet',
			'controller' => 'AppController',
			'action' => 'tweet'
		);

		$routes['comprar'] = array(
			'route' => '/comprar',
			'controller' => 'AppController',
			'action' => 'comprar'
		);

		
		$routes['verificarEmail'] = array(
			'route' => '/verificarEmail',
			'controller' => 'AppController',
			'action' => 'verificarEmail'
		);

		$routes['ativarConta'] = array(
			'route' => '/ativarConta',
			'controller' => 'AppController',
			'action' => 'ativarConta'
		);

		$routes['atualizarDados'] = array(
			'route' => '/atualizar_dados',
			'controller' => 'AppController',
			'action' => 'atualizarDados'
		);

		$routes['verificaNome'] = array(
			'route' => '/verifica_nome',
			'controller' => 'AppController',
			'action' => 'verificaNome'
		);

		$routes['contaDesativada'] = array(
			'route' => '/contaDesativada',
			'controller' => 'AppController',
			'action' => 'contaDesativada'
		);

		$routes['quem_seguir'] = array(
			'route' => '/quem_seguir',
			'controller' => 'AppController',
			'action' => 'quemSeguir'
		);

		$routes['acao'] = array(
			'route' => '/acao',
			'controller' => 'AppController',
			'action' => 'acao'
		);

		//AuthController

		$routes['autenticar'] = array(
			'route' => '/autenticar',
			'controller' => 'AuthController',
			'action' => 'autenticar'
		);

		$routes['sair'] = array(
			'route' => '/sair',
			'controller' => 'AuthController',
			'action' => 'sair'
		);

		//IndexController
		$routes['home'] = array(
			'route' => '/',
			'controller' => 'indexController',
			'action' => 'index'
		);

		$routes['land'] = array(
			'route' => '/land',
			'controller' => 'indexController',
			'action' => 'land'
		);

		$routes['loja'] = array(
			'route' => '/loja',
			'controller' => 'indexController',
			'action' => 'loja'
		);

		$routes['contato'] = array(
			'route' => '/contato',
			'controller' => 'indexController',
			'action' => 'contato'
		);

		$routes['regulamento'] = array(
			'route' => '/regulamento',
			'controller' => 'indexController',
			'action' => 'regulamento'
		);

		$routes['sobre'] = array(
			'route' => '/sobre',
			'controller' => 'indexController',
			'action' => 'sobre'
		);

		$routes['produto'] = array(
			'route' => '/produto',
			'controller' => 'indexController',
			'action' => 'produto'
		);

		$routes['inscreverse'] = array(
			'route' => '/inscreverse',
			'controller' => 'indexController',
			'action' => 'inscreverse'
		);

		$routes['registrar'] = array(
			'route' => '/registrar',
			'controller' => 'indexController',
			'action' => 'registrar'
		);

		$routes['entrar'] = array(
			'route' => '/entrar',
			'controller' => 'indexController',
			'action' => 'entrar'
		);

		$routes['recuperarSenha'] = array(
			'route' => '/recuperarSenha',
			'controller' => 'indexController',
			'action' => 'recuperarSenha'
		);

		$routes['atualizarSenha'] = array(
			'route' => '/atualizarSenha',
			'controller' => 'indexController',
			'action' => 'atualizarSenha'
		);

		$routes['enviarEmailContato'] = array(
			'route' => '/enviarEmailContato',
			'controller' => 'indexController',
			'action' => 'enviarEmailContato'
		);

		$routes['endpointTeste'] = array(
			'route' => '/endpointTeste',
			'controller' => 'indexController',
			'action' => 'endpointTeste'
		);

		$this->setRoutes($routes);
	}

}

?>