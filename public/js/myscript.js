// meus_jogos.phtml
$(".btn-steamguard").on("click", e => {

    let id = $(e.target).val()

    $.ajax({
        type: 'GET',
        url: "/obter_codigo",
        data: `code=${id}`,
        success: dados => {
            $(`.${id}`).html(dados);
            console.log(dados)
        },
        error: function() {
            $(`.${id}`).html("Erro");
        }
    })
});

//Mostra o ícone de carregando
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading"); },
    ajaxStop: function() { $body.removeClass("loading"); }
});

$(document).ready(function() {
    $('#cardExpirationMonth').mask('00');
    $('#cardExpirationYear').mask('0000');
    $('#securityCode').mask('000');
    $('#cardNumber').mask('0000000000000000');
    $('#docNumber').mask('00000000000', { reverse: true });
});

if (window.location.pathname == '/meus_jogos') {
    var formFiles, divReturn, progressBar;

    formFiles = document.getElementById('formFiles');
    divReturn = document.getElementById('return');
    progressBar = document.getElementById('progressBar');

    formFiles.addEventListener('submit', sendForm, false);

    function sendForm(evt) {


        var formData, ajax, pct;

        formData = new FormData(evt.target);

        ajax = new XMLHttpRequest();

        ajax.onreadystatechange = function() {

            if (ajax.readyState == 4) {
                formFiles.reset();
                divReturn.textContent = ajax.response;
                progressBar.style.display = 'none';
            } else {
                progressBar.style.display = 'block';
                divReturn.style.display = 'block';
                divReturn.textContent = 'Enviando arquivo!';
            }
        }

        ajax.upload.addEventListener('progress', function(evt) {

            pct = Math.floor((evt.loaded * 100) / evt.total);
            progressBar.style.width = pct + '%';
            progressBar.getElementsByTagName('span')[0].textContent = pct + '%';

        }, false);

        ajax.open('POST', 'enviarComprovante');
        ajax.send(formData);
    }
}




$("a").on("click", () => {
    let dados = $('a').serialize();
    console.log(dados)
});

$('#aplicar').on('click', () => {
    var $cupom = document.querySelector('#cupom').value
    $.ajax({
        type: 'GET',
        url: '/verificaCupom',
        data: `cupom=${$cupom}`,
        success: dados => { location.replace(dados) },
        error: dados => { erro }
    })


    //console.log('Cupom: ' + $cupom)
    //$('.container').load('/verificaCupom?cupom='+$cupom)
})



function aceitaTermo() {
    if ($('#checkTermo').prop("checked") == true) {
        $('#btnAceitarProsseguir').prop('disabled', false)
    } else {
        $('#btnAceitarProsseguir').prop('disabled', true)
    }
}

function verificaNome() {
    $('#enviaDadosPerfil').prop('disabled', true)
    let nick = $('#nickname').val()
    var nickFinal = ''
    if (nick.length >= 3) {
        console.log(nick.length)
        $.ajax({
            type: 'GET',
            url: '/verifica_nome',
            data: `nome=${nick}`,
            success: dados => {
                $('#info').html(dados),
                    console.log(dados),
                    $('#enviaDadosPerfil').prop('disabled', false),
                    verificaEspaco(nick)

            },
            error: erro => { console.log(erro) }
        })
    }

}

function verificaEspaco(nick) {
    apelido = nick
    var nickFinal = ''
    if (apelido.search('[ \f\n\r\t\v​\u00A0\u1680​\u180e\u2000​\u2001\u2002​\u2003\u2004​\u2005\u2006​\u2007\u2008​\u2009\u200a​\u2028\u2029​\u2028\u2029​\u202f\u205f​\u3000]') != -1) {
        alert('Nick não pode conter espaço')
        nickFinal = apelido.replace(' ', '')
        $("#nickname").val(nickFinal);
    }
}

$('#linkAfiliado').click(function() {
    //Visto que o 'copy' copia o texto que estiver selecionado, talvez você queira colocar seu valor em um txt escondido
    var a = $('#linkAfiliado').select();
    try {
        var ok = document.execCommand('copy');
        if (ok) { alert('Link copiado para a área de transferência'); }
    } catch (e) {
        alert(e)
    }
});

function getPublicKey() {
    var key = $('#publicKey').val()
    return key
}

(function(win, doc) {
    "use strict"
    //Public key
    var key = getPublicKey()
    if (!key) {
        key = 'APP_USR-c355c068-0163-4426-a284-1a6fcd558604'
    }
    window.Mercadopago.setPublishableKey(key);
    window.Mercadopago.getIdentificationTypes();
    //Card bin 6-digitos
    function cardBin(event) {
        let textLength = event.target.value.length
        if (textLength >= 6) {
            let bin = event.target.value.substring(0, 6)
            window.Mercadopago.getPaymentMethod({
                "bin": bin
            }, setPaymentMethodInfo);

            Mercadopago.getInstallments({
                "bin": bin,
                "amount": parseFloat(document.querySelector('#amount').value),
            }, setInstallmentInfo)

        }
    }

    if (doc.querySelector('#cardNumber')) {
        let cardNumber = doc.querySelector('#cardNumber');
        cardNumber.addEventListener('keyup', cardBin, false)
    }

    //Set Payment Method
    function setPaymentMethodInfo(status, response) {
        if (status == 200) {
            const paymentMethodElement = document.querySelector('input[name=paymentMethodId]')
            paymentMethodElement.value = response[0].id
            var https = "<img src='" + response[0].thumbnail + "' alt=''Bandeira do Cartão'>"
            doc.querySelector('.brand').innerHTML = https.replace("http", "https")
        } else {
            alert(`payment method info error: ${response}`);
        }
    };

    //Set Installments
    function setInstallmentInfo(status, response) {
        let label = response[0].payer_costs
        let InstallmentsSel = doc.querySelector('#installments')
        InstallmentsSel.options.length = 0

        label.map(function(elem, ind, obj) {
            //console.log(response)
            let txtOpt = elem.recommended_message
            let valOpt = elem.installments
            InstallmentsSel.options[InstallmentsSel.options.length] = new Option(txtOpt, valOpt)
        })
    };

    //Create Token - 4235 6477 2802 5682
    function sendPayment(event) {
        event.preventDefault()
        window.Mercadopago.createToken(event.target, sdkResponseHandler);
    }

    function sdkResponseHandler(status, response) {
        console.log(response)
        console.log(status)
        if (status == 200 || status == 201) {
            let form = doc.querySelector('#pay');
            let card = doc.createElement('input');
            card.setAttribute('name', 'token');
            card.setAttribute('type', 'text');
            card.setAttribute('value', response.id);
            form.appendChild(card);
            form.submit();
        }
    }
    if (doc.querySelector('#pay')) {
        let formPay = doc.querySelector('#pay')
        formPay.addEventListener('submit', sendPayment, false)
    }

})(window, document);